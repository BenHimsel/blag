---
author: "Ben"
title: "About the Blag"
date: 2021-11-12T21:04:53Z
description: "About the Blag"
tags: 
    - "crappost"
    - "about"
featured: true
---

# Blag

Welcome to a crappy blog using [Hugo](https://gohugo.io/getting-started/usage/) using the [klakegg/docker-hugo](https://github.com/klakegg/docker-hugo) docker image with the [PaperMod theme](https://github.com/adityatelange/hugo-PaperMod/wiki/Installation#method-3)

## Structure

Site content is stored in [GitLab](https://gitlab.com/) the project's [content folder](https://gitlab.com/BenHimsel/blag/-/tree/master/content/) using [Markdown](https://www.markdownguide.org/)

## Build

The site is built in a Dockerfile using a [multistage build](https://docs.docker.com/develop/develop-images/multistage-build/).

The build is executed using [Kaniko](https://github.com/GoogleContainerTools/kaniko) in [GitLab CI](https://docs.gitlab.com/ee/ci/) defined in the [CI YAML file](https://gitlab.com/BenHimsel/blag/-/blob/master/.gitlab-ci.yml) building to the project's [GitLab Container Registry](https://gitlab.com/BenHimsel/blag/container_registry/).

## Deployment

### Platform

The Kubernetes Cluster was provisioned using [Vultr Kubernetes Engine](https://www.vultr.com/docs/vultr-kubernetes-engine#How_to_Deploy_a_VKE_Cluster).

The services inside the cluster are [Helm Charts](https://gitlab.com/sniffleheim/cluster/) deployed by GitLab by installing the [GitLab Agent](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html).

Ingress is done by [HAProxy](https://artifacthub.io/packages/helm/haproxytech/kubernetes-ingress) as DaemonSet. With the Kubernetes Service set to the type LoadBalancer to cause Vultr to provision a Load Balancer.

![Networking](networking.drawio.svg)

### Application Deployment

The application itself is deployed using [Kustomized](https://kustomize.io/) Kubernetes [manifests](https://gitlab.com/BenHimsel/blag/-/tree/master/manifests) pushed using GitLab CI/CD with the GitLab Agent.