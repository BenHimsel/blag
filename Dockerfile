FROM busybox:latest as theme

WORKDIR /theme
RUN TMPFILE=`mktemp` && wget "https://github.com/adityatelange/hugo-PaperMod/archive/master.zip" -O $TMPFILE && unzip -d /theme $TMPFILE

FROM klakegg/hugo:latest AS build

RUN hugo new site "diabetes-and-kubernetes" -f yml

COPY --from=theme /theme/hugo-PaperMod-master/ /src/diabetes-and-kubernetes/themes/PaperMod/
COPY config.yml /src/diabetes-and-kubernetes/
WORKDIR /src/diabetes-and-kubernetes/
COPY /content/ /src/diabetes-and-kubernetes/content/
RUN hugo -D --config /src/diabetes-and-kubernetes/config.yml

FROM nginx:latest

COPY --from=build /src/diabetes-and-kubernetes/public/ /usr/share/nginx/html/